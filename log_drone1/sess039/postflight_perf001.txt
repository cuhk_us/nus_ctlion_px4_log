PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 7562 events
sf0x_comms_errors: 6 events
sf0x_read: 7570 events, 653016us elapsed, 86us avg, min 13us max 3014us
mavlink_txe: 56 events
mavlink_el: 98736 events, 20985918us elapsed, 212us avg, min 63us max 9237us
mc_att_control: 49034 events, 9681152us elapsed, 197us avg, min 48us max 717us
attitude_estimator_ekf: 49046 events, 38376245us elapsed, 782us avg, min 582us max 1426us
sensor task update: 49061 events, 5759075us elapsed, 117us avg, min 64us max 841us
lsm303d_accel_resched: 8694 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 24630 events, 896010us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 196312 events, 7644309us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 187337 events, 13302439us elapsed, 71us avg, min 69us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 49280 events, 147941us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 14580 events
ms5611_comms_errors: 0 events
ms5611_measure: 19446 events, 558798us elapsed, 28us avg, min 14us max 3849us
ms5611_read: 19445 events, 2947409us elapsed, 151us avg, min 23us max 4694us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 49031 events, 40433403us elapsed, 824us avg, min 283us max 12826us
io_badidle  : 0 events
io_idle     : 110084 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 110093 events, 1353838us elapsed, 12us avg, min 9us max 1651us
io_txns     : 110094 events, 37197939us elapsed, 337us avg, min 148us max 11272us
DMA allocations: 9 events
