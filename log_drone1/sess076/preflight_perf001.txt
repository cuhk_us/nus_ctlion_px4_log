PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 64528 events
sf0x_comms_errors: 6 events
sf0x_read: 64537 events, 5725542us elapsed, 88us avg, min 13us max 32808us
mavlink_txe: 75 events
mavlink_el: 821907 events, 167532120us elapsed, 203us avg, min 64us max 17616us
mc_att_control: 416747 events, 79676395us elapsed, 191us avg, min 46us max 647us
attitude_estimator_ekf: 416759 events, 327977686us elapsed, 786us avg, min 582us max 1592us
sensor task update: 416775 events, 45547102us elapsed, 109us avg, min 73us max 614us
lsm303d_accel_resched: 128060 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 209647 events, 7539072us elapsed, 35us avg, min 35us max 37us
lsm303d_accel_read: 1666373 events, 63613212us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 1594305 events, 112959815us elapsed, 70us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 419310 events, 1263484us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 122740 events
ms5611_comms_errors: 0 events
ms5611_measure: 163659 events, 4557697us elapsed, 27us avg, min 14us max 8332us
ms5611_read: 163659 events, 24183834us elapsed, 147us avg, min 23us max 7969us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 416747 events, 284663134us elapsed, 683us avg, min 283us max 44009us
io_badidle  : 0 events
io_idle     : 787621 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 787625 events, 9447097us elapsed, 11us avg, min 9us max 3086us
io_txns     : 787626 events, 260731108us elapsed, 331us avg, min 147us max 41273us
DMA allocations: 8 events
