PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 22861 events
sf0x_comms_errors: 5 events
sf0x_read: 22868 events, 2053659us elapsed, 89us avg, min 13us max 3951us
mavlink_txe: 95 events
mavlink_el: 295086 events, 60262655us elapsed, 204us avg, min 63us max 16275us
mc_att_control: 147218 events, 29420652us elapsed, 199us avg, min 47us max 731us
attitude_estimator_ekf: 147228 events, 114511954us elapsed, 777us avg, min 582us max 1447us
sensor task update: 147244 events, 17243599us elapsed, 117us avg, min 64us max 526us
lsm303d_accel_resched: 39830 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 74042 events, 2692383us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 588977 events, 22937343us elapsed, 38us avg, min 38us max 40us
mpu6000_bad_transfers: 0 events
mpu6000_read: 563088 events, 39978926us elapsed, 70us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 4 events
adc_samples: 148100 events, 444063us elapsed, 2us avg, min 2us max 4us
ms5611_buffer_overflows: 43728 events
ms5611_comms_errors: 0 events
ms5611_measure: 58310 events, 1650215us elapsed, 28us avg, min 14us max 9372us
ms5611_read: 58310 events, 8976609us elapsed, 153us avg, min 23us max 5133us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 147217 events, 122181283us elapsed, 829us avg, min 283us max 12135us
io_badidle  : 0 events
io_idle     : 332465 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 332470 events, 4052031us elapsed, 12us avg, min 9us max 2663us
io_txns     : 332471 events, 112402619us elapsed, 338us avg, min 148us max 11273us
DMA allocations: 11 events
