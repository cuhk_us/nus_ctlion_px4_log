PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 8117 events
sf0x_comms_errors: 6 events
sf0x_read: 8125 events, 700984us elapsed, 86us avg, min 13us max 3103us
mavlink_txe: 58 events
mavlink_el: 105756 events, 22524451us elapsed, 212us avg, min 63us max 9237us
mc_att_control: 52623 events, 10386169us elapsed, 197us avg, min 48us max 717us
attitude_estimator_ekf: 52634 events, 41215226us elapsed, 783us avg, min 582us max 1496us
sensor task update: 52651 events, 6182060us elapsed, 117us avg, min 64us max 841us
lsm303d_accel_resched: 9553 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 26434 events, 961609us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 210667 events, 8203202us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 201050 events, 14276144us elapsed, 71us avg, min 69us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 4 events
adc_samples: 52880 events, 158736us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 15631 events
ms5611_comms_errors: 0 events
ms5611_measure: 20848 events, 600524us elapsed, 28us avg, min 14us max 3849us
ms5611_read: 20847 events, 3199772us elapsed, 153us avg, min 23us max 4694us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 52620 events, 43410461us elapsed, 824us avg, min 283us max 12826us
io_badidle  : 0 events
io_idle     : 118151 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 118157 events, 1452301us elapsed, 12us avg, min 9us max 1651us
io_txns     : 118158 events, 39935477us elapsed, 337us avg, min 148us max 11272us
DMA allocations: 11 events
