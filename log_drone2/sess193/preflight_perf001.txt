PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 36650 events
sf0x_comms_errors: 6 events
sf0x_read: 36658 events, 2930240us elapsed, 79us avg, min 13us max 4990us
mavlink_txe: 92 events
mavlink_el: 476517 events, 95665349us elapsed, 200us avg, min 62us max 17081us
mc_att_control: 238113 events, 42202894us elapsed, 177us avg, min 46us max 642us
attitude_estimator_ekf: 238122 events, 193719597us elapsed, 813us avg, min 581us max 1610us
sensor task update: 238139 events, 29433612us elapsed, 123us avg, min 66us max 579us
lsm303d_accel_resched: 1941 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 119332 events, 4292251us elapsed, 35us avg, min 35us max 37us
lsm303d_accel_read: 954478 events, 36478089us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 907498 events, 64287590us elapsed, 70us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 6 events
adc_samples: 238680 events, 718953us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 70281 events
ms5611_comms_errors: 0 events
ms5611_measure: 93714 events, 2674829us elapsed, 28us avg, min 14us max 4477us
ms5611_read: 93714 events, 13894045us elapsed, 148us avg, min 23us max 5311us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 238110 events, 190178722us elapsed, 798us avg, min 279us max 15475us
io_badidle  : 0 events
io_idle     : 517395 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 517396 events, 6420203us elapsed, 12us avg, min 9us max 1790us
io_txns     : 517398 events, 174624422us elapsed, 337us avg, min 147us max 12029us
DMA allocations: 17 events
