PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 24538 events
sf0x_comms_errors: 6 events
sf0x_read: 24546 events, 1903029us elapsed, 77us avg, min 13us max 4990us
mavlink_txe: 81 events
mavlink_el: 321296 events, 64052781us elapsed, 199us avg, min 62us max 17081us
mc_att_control: 159371 events, 27880239us elapsed, 174us avg, min 46us max 642us
attitude_estimator_ekf: 159380 events, 129253480us elapsed, 810us avg, min 581us max 1569us
sensor task update: 159398 events, 19579495us elapsed, 122us avg, min 66us max 579us
lsm303d_accel_resched: 1282 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 79875 events, 2873134us elapsed, 35us avg, min 35us max 37us
lsm303d_accel_read: 638886 events, 24415086us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 607448 events, 43032114us elapsed, 70us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 5 events
adc_samples: 159770 events, 481198us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 47188 events
ms5611_comms_errors: 0 events
ms5611_measure: 62923 events, 1740899us elapsed, 27us avg, min 14us max 4243us
ms5611_read: 62922 events, 9145858us elapsed, 145us avg, min 23us max 5311us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 159369 events, 126114829us elapsed, 791us avg, min 283us max 15475us
io_badidle  : 0 events
io_idle     : 342954 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 342959 events, 4242526us elapsed, 12us avg, min 9us max 1790us
io_txns     : 342959 events, 115829383us elapsed, 337us avg, min 147us max 12029us
DMA allocations: 14 events
