PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 27195 events
sf0x_comms_errors: 6 events
sf0x_read: 27203 events, 2131953us elapsed, 78us avg, min 13us max 4990us
mavlink_txe: 81 events
mavlink_el: 354933 events, 70985521us elapsed, 199us avg, min 62us max 17081us
mc_att_control: 176648 events, 31370366us elapsed, 177us avg, min 46us max 642us
attitude_estimator_ekf: 176656 events, 143434260us elapsed, 811us avg, min 581us max 1569us
sensor task update: 176674 events, 21742060us elapsed, 123us avg, min 66us max 579us
lsm303d_accel_resched: 1432 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 88532 events, 3184464us elapsed, 35us avg, min 35us max 37us
lsm303d_accel_read: 708123 events, 27062060us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 673274 events, 47696405us elapsed, 70us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 5 events
adc_samples: 177080 events, 533394us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 52232 events
ms5611_comms_errors: 0 events
ms5611_measure: 69649 events, 1940379us elapsed, 27us avg, min 14us max 4243us
ms5611_read: 69649 events, 10208979us elapsed, 146us avg, min 23us max 5311us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 176644 events, 140125234us elapsed, 793us avg, min 280us max 15475us
io_badidle  : 0 events
io_idle     : 381122 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 381123 events, 4717325us elapsed, 12us avg, min 9us max 1790us
io_txns     : 381130 events, 128681637us elapsed, 337us avg, min 147us max 12029us
DMA allocations: 15 events
