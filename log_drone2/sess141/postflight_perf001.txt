PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 13056 events
sf0x_comms_errors: 6 events
sf0x_read: 13064 events, 1071111us elapsed, 81us avg, min 13us max 4537us
mavlink_txe: 64 events
mavlink_el: 170298 events, 33885680us elapsed, 198us avg, min 62us max 28273us
mc_att_control: 84800 events, 15319905us elapsed, 180us avg, min 47us max 732us
attitude_estimator_ekf: 84809 events, 69415312us elapsed, 818us avg, min 586us max 1401us
sensor task update: 84825 events, 10568253us elapsed, 124us avg, min 72us max 587us
lsm303d_accel_resched: 782 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 42512 events, 1545583us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 340023 events, 13241862us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 323321 events, 22965522us elapsed, 71us avg, min 69us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 85040 events, 255031us elapsed, 2us avg, min 2us max 4us
ms5611_buffer_overflows: 25080 events
ms5611_comms_errors: 0 events
ms5611_measure: 33446 events, 929371us elapsed, 27us avg, min 14us max 3184us
ms5611_read: 33446 events, 4976232us elapsed, 148us avg, min 23us max 4879us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 84796 events, 67665294us elapsed, 797us avg, min 283us max 7345us
io_badidle  : 0 events
io_idle     : 184741 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 184750 events, 2327217us elapsed, 12us avg, min 9us max 3312us
io_txns     : 184751 events, 62072821us elapsed, 335us avg, min 148us max 5389us
DMA allocations: 9 events
