PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 55495 events
sf0x_comms_errors: 6 events
sf0x_read: 55503 events, 4678133us elapsed, 84us avg, min 13us max 9752us
mavlink_txe: 112 events
mavlink_el: 708942 events, 150710229us elapsed, 212us avg, min 63us max 14704us
mc_att_control: 361191 events, 68839700us elapsed, 190us avg, min 48us max 656us
attitude_estimator_ekf: 361200 events, 299373324us elapsed, 828us avg, min 587us max 1594us
sensor task update: 361217 events, 43588332us elapsed, 120us avg, min 66us max 515us
lsm303d_accel_resched: 3380 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 181015 events, 6579615us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 1447810 events, 56383104us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 1376574 events, 97792440us elapsed, 71us avg, min 69us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 362050 events, 1086083us elapsed, 2us avg, min 2us max 4us
ms5611_buffer_overflows: 105784 events
ms5611_comms_errors: 0 events
ms5611_measure: 141052 events, 4075745us elapsed, 28us avg, min 14us max 4334us
ms5611_read: 141052 events, 21598008us elapsed, 153us avg, min 23us max 8582us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 361185 events, 263416755us elapsed, 729us avg, min 282us max 71127us
io_badidle  : 0 events
io_idle     : 721179 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 721187 events, 9090194us elapsed, 12us avg, min 9us max 1915us
io_txns     : 721188 events, 240852736us elapsed, 333us avg, min 140us max 68539us
DMA allocations: 9 events
