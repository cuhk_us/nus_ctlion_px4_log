PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 10463 events
sf0x_comms_errors: 6 events
sf0x_read: 10471 events, 831284us elapsed, 79us avg, min 13us max 3628us
mavlink_txe: 58 events
mavlink_el: 136654 events, 27277802us elapsed, 199us avg, min 62us max 15441us
mc_att_control: 67934 events, 12200590us elapsed, 179us avg, min 47us max 810us
attitude_estimator_ekf: 67944 events, 55138794us elapsed, 811us avg, min 582us max 1475us
sensor task update: 67960 events, 8563967us elapsed, 126us avg, min 64us max 840us
lsm303d_accel_resched: 3098 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 34060 events, 1239160us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 272210 events, 10598707us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 259044 events, 18418767us elapsed, 71us avg, min 68us max 84us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 4 events
adc_samples: 68140 events, 204560us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 20142 events
ms5611_comms_errors: 0 events
ms5611_measure: 26862 events, 789246us elapsed, 29us avg, min 14us max 3863us
ms5611_read: 26861 events, 3884894us elapsed, 144us avg, min 23us max 3994us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 67927 events, 55477453us elapsed, 816us avg, min 283us max 12822us
io_badidle  : 0 events
io_idle     : 150977 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 150980 events, 1927257us elapsed, 12us avg, min 9us max 7536us
io_txns     : 150981 events, 50930333us elapsed, 337us avg, min 148us max 11265us
DMA allocations: 11 events
