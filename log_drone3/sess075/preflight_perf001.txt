PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 1930 events
sf0x_comms_errors: 6 events
sf0x_read: 1938 events, 149065us elapsed, 76us avg, min 13us max 2927us
mavlink_txe: 53 events
mavlink_el: 25888 events, 5237993us elapsed, 202us avg, min 63us max 7821us
mc_att_control: 12632 events, 2181721us elapsed, 172us avg, min 48us max 802us
attitude_estimator_ekf: 12640 events, 10096820us elapsed, 798us avg, min 582us max 1346us
sensor task update: 12656 events, 1646118us elapsed, 130us avg, min 72us max 747us
lsm303d_accel_resched: 578 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 6349 events, 231034us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 50739 events, 1975763us elapsed, 38us avg, min 38us max 41us
mpu6000_bad_transfers: 0 events
mpu6000_read: 48313 events, 3435311us elapsed, 71us avg, min 69us max 81us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 12710 events, 38141us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 3777 events
ms5611_comms_errors: 0 events
ms5611_measure: 5042 events, 143405us elapsed, 28us avg, min 14us max 3300us
ms5611_read: 5041 events, 711006us elapsed, 141us avg, min 23us max 3182us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 12626 events, 10432673us elapsed, 826us avg, min 283us max 4151us
io_badidle  : 0 events
io_idle     : 28507 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 28513 events, 355943us elapsed, 12us avg, min 9us max 1189us
io_txns     : 28514 events, 9604052us elapsed, 336us avg, min 148us max 2168us
DMA allocations: 8 events
