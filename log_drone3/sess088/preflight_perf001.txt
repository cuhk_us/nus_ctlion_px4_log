PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 9535 events
sf0x_comms_errors: 5 events
sf0x_read: 9542 events, 731268us elapsed, 76us avg, min 13us max 3739us
mavlink_txe: 87 events
mavlink_el: 124671 events, 25092835us elapsed, 201us avg, min 62us max 17754us
mc_att_control: 61960 events, 10827428us elapsed, 174us avg, min 47us max 657us
attitude_estimator_ekf: 61971 events, 50284070us elapsed, 811us avg, min 582us max 1439us
sensor task update: 61986 events, 7710516us elapsed, 124us avg, min 64us max 558us
lsm303d_accel_resched: 3013 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 31066 events, 1130041us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 248263 events, 9666440us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 236271 events, 16790334us elapsed, 71us avg, min 69us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 62150 events, 186560us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 18364 events
ms5611_comms_errors: 0 events
ms5611_measure: 24492 events, 684124us elapsed, 27us avg, min 14us max 3237us
ms5611_read: 24491 events, 3541731us elapsed, 144us avg, min 23us max 3989us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 61958 events, 48885849us elapsed, 789us avg, min 283us max 5535us
io_badidle  : 0 events
io_idle     : 133428 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 133435 events, 1703014us elapsed, 12us avg, min 9us max 1270us
io_txns     : 133436 events, 44834546us elapsed, 336us avg, min 148us max 2814us
DMA allocations: 8 events
