PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 7048 events
sf0x_comms_errors: 6 events
sf0x_read: 7056 events, 576862us elapsed, 81us avg, min 13us max 3611us
mavlink_txe: 64 events
mavlink_el: 91318 events, 18785512us elapsed, 205us avg, min 62us max 14725us
mc_att_control: 45928 events, 8415836us elapsed, 183us avg, min 46us max 724us
attitude_estimator_ekf: 45938 events, 37473650us elapsed, 815us avg, min 582us max 1534us
sensor task update: 45955 events, 5801520us elapsed, 126us avg, min 66us max 766us
lsm303d_accel_resched: 2061 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 23036 events, 828512us elapsed, 35us avg, min 35us max 37us
lsm303d_accel_read: 184106 events, 7036384us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 175209 events, 12417264us elapsed, 70us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 4 events
adc_samples: 46090 events, 139024us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 13545 events
ms5611_comms_errors: 0 events
ms5611_measure: 18066 events, 541800us elapsed, 29us avg, min 14us max 4706us
ms5611_read: 18065 events, 2668458us elapsed, 147us avg, min 23us max 9497us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 45922 events, 37644867us elapsed, 819us avg, min 283us max 16368us
io_badidle  : 0 events
io_idle     : 102374 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 102375 events, 1287814us elapsed, 12us avg, min 8us max 2477us
io_txns     : 102376 events, 34576477us elapsed, 337us avg, min 148us max 11202us
DMA allocations: 12 events
