PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 77457 events
sf0x_comms_errors: 6 events
sf0x_read: 77465 events, 5758795us elapsed, 74us avg, min 13us max 3655us
mavlink_txe: 77 events
mavlink_el: 1026282 events, 201544232us elapsed, 196us avg, min 62us max 9844us
mc_att_control: 501449 events, 87168514us elapsed, 173us avg, min 47us max 709us
attitude_estimator_ekf: 501459 events, 406282073us elapsed, 810us avg, min 583us max 1564us
sensor task update: 501476 events, 57638288us elapsed, 114us avg, min 64us max 456us
lsm303d_accel_resched: 22920 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 251262 events, 9144615us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 2008097 events, 78200002us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 1910772 events, 135831701us elapsed, 71us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 502540 events, 1508630us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 149662 events
ms5611_comms_errors: 0 events
ms5611_measure: 199555 events, 5368228us elapsed, 26us avg, min 14us max 3914us
ms5611_read: 199554 events, 27691831us elapsed, 138us avg, min 23us max 6719us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 501447 events, 338682888us elapsed, 675us avg, min 283us max 119723us
io_badidle  : 0 events
io_idle     : 935945 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 935952 events, 11706007us elapsed, 12us avg, min 9us max 2446us
io_txns     : 935954 events, 309159114us elapsed, 330us avg, min 140us max 119410us
DMA allocations: 8 events
