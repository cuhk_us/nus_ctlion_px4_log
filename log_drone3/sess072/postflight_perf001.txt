PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 80189 events
sf0x_comms_errors: 6 events
sf0x_read: 80197 events, 5974161us elapsed, 74us avg, min 13us max 4326us
mavlink_txe: 84 events
mavlink_el: 1060814 events, 208742357us elapsed, 196us avg, min 62us max 10310us
mc_att_control: 519189 events, 90888929us elapsed, 175us avg, min 47us max 709us
attitude_estimator_ekf: 519199 events, 420989453us elapsed, 810us avg, min 583us max 1564us
sensor task update: 519215 events, 59869929us elapsed, 115us avg, min 64us max 456us
lsm303d_accel_resched: 23720 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 260152 events, 9467758us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 2079139 events, 80966144us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 1978374 events, 140639554us elapsed, 71us avg, min 68us max 81us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 520320 events, 1561977us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 154857 events
ms5611_comms_errors: 0 events
ms5611_measure: 206482 events, 5558047us elapsed, 26us avg, min 14us max 3914us
ms5611_read: 206482 events, 28767729us elapsed, 139us avg, min 23us max 6719us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 519185 events, 353138565us elapsed, 680us avg, min 283us max 119723us
io_badidle  : 0 events
io_idle     : 975185 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 975191 events, 12218083us elapsed, 12us avg, min 9us max 4732us
io_txns     : 975194 events, 322414108us elapsed, 330us avg, min 140us max 119410us
DMA allocations: 9 events
