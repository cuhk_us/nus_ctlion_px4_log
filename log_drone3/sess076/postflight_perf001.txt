PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 11943 events
sf0x_comms_errors: 6 events
sf0x_read: 11952 events, 938041us elapsed, 78us avg, min 13us max 3834us
mavlink_txe: 67 events
mavlink_el: 154812 events, 31368835us elapsed, 202us avg, min 62us max 14078us
mc_att_control: 77658 events, 13993131us elapsed, 180us avg, min 48us max 782us
attitude_estimator_ekf: 77668 events, 63336543us elapsed, 815us avg, min 582us max 1528us
sensor task update: 77684 events, 9814578us elapsed, 126us avg, min 64us max 840us
lsm303d_accel_resched: 3771 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 38932 events, 1415495us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 311127 events, 12112961us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 296091 events, 21043614us elapsed, 71us avg, min 68us max 84us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 77880 events, 233666us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 22912 events
ms5611_comms_errors: 0 events
ms5611_measure: 30556 events, 907406us elapsed, 29us avg, min 14us max 8186us
ms5611_read: 30556 events, 4521641us elapsed, 147us avg, min 23us max 4978us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 77655 events, 63507335us elapsed, 817us avg, min 283us max 12820us
io_badidle  : 0 events
io_idle     : 172630 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 172636 events, 2198905us elapsed, 12us avg, min 9us max 1854us
io_txns     : 172639 events, 58294265us elapsed, 337us avg, min 148us max 11267us
DMA allocations: 9 events
