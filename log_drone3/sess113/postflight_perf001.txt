PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 12973 events
sf0x_comms_errors: 6 events
sf0x_read: 12981 events, 1028864us elapsed, 79us avg, min 13us max 3805us
mavlink_txe: 65 events
mavlink_el: 169192 events, 33995653us elapsed, 200us avg, min 62us max 11036us
mc_att_control: 84203 events, 15207812us elapsed, 180us avg, min 48us max 812us
attitude_estimator_ekf: 84214 events, 68429631us elapsed, 812us avg, min 582us max 1532us
sensor task update: 84230 events, 10572209us elapsed, 125us avg, min 64us max 826us
lsm303d_accel_resched: 3941 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 42210 events, 1535457us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 337339 events, 13133060us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 321023 events, 22822279us elapsed, 71us avg, min 69us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 84440 events, 253384us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 24952 events
ms5611_comms_errors: 0 events
ms5611_measure: 33275 events, 903711us elapsed, 27us avg, min 14us max 3369us
ms5611_read: 33275 events, 4896697us elapsed, 147us avg, min 23us max 4898us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 84199 events, 68623335us elapsed, 815us avg, min 283us max 12818us
io_badidle  : 0 events
io_idle     : 186872 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 186878 events, 2360853us elapsed, 12us avg, min 9us max 4387us
io_txns     : 186879 events, 62947094us elapsed, 336us avg, min 148us max 11267us
DMA allocations: 9 events
