PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 7137 events
sf0x_comms_errors: 6 events
sf0x_read: 7145 events, 516898us elapsed, 72us avg, min 13us max 3026us
mavlink_txe: 59 events
mavlink_el: 94906 events, 18495703us elapsed, 194us avg, min 62us max 20614us
mc_att_control: 46348 events, 7941102us elapsed, 171us avg, min 46us max 720us
attitude_estimator_ekf: 46359 events, 37305931us elapsed, 804us avg, min 582us max 1353us
sensor task update: 46375 events, 5803011us elapsed, 125us avg, min 64us max 772us
lsm303d_accel_resched: 2179 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 23244 events, 836122us elapsed, 35us avg, min 35us max 37us
lsm303d_accel_read: 185762 events, 7097795us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 176790 events, 12525099us elapsed, 70us avg, min 68us max 84us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 46510 events, 140033us elapsed, 3us avg, min 2us max 4us
ms5611_buffer_overflows: 13841 events
ms5611_comms_errors: 0 events
ms5611_measure: 18462 events, 507885us elapsed, 27us avg, min 14us max 2728us
ms5611_read: 18461 events, 2608982us elapsed, 141us avg, min 23us max 4071us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 46344 events, 37762204us elapsed, 814us avg, min 283us max 14296us
io_badidle  : 0 events
io_idle     : 103174 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 103176 events, 1287648us elapsed, 12us avg, min 9us max 2474us
io_txns     : 103179 events, 34691352us elapsed, 336us avg, min 146us max 11214us
DMA allocations: 8 events
