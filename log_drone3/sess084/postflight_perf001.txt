PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 9747 events
sf0x_comms_errors: 5 events
sf0x_read: 9754 events, 753293us elapsed, 77us avg, min 13us max 4003us
mavlink_txe: 63 events
mavlink_el: 126976 events, 25659054us elapsed, 202us avg, min 62us max 9416us
mc_att_control: 63277 events, 11455915us elapsed, 181us avg, min 48us max 812us
attitude_estimator_ekf: 63285 events, 51420090us elapsed, 812us avg, min 582us max 1510us
sensor task update: 63301 events, 7957501us elapsed, 125us avg, min 64us max 832us
lsm303d_accel_resched: 2899 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 31725 events, 1154615us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 253543 events, 9870817us elapsed, 38us avg, min 38us max 41us
mpu6000_bad_transfers: 0 events
mpu6000_read: 241283 events, 17156918us elapsed, 71us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 3 events
adc_samples: 63470 events, 190351us elapsed, 2us avg, min 2us max 4us
ms5611_buffer_overflows: 18739 events
ms5611_comms_errors: 0 events
ms5611_measure: 24993 events, 732326us elapsed, 29us avg, min 14us max 3903us
ms5611_read: 24992 events, 3639725us elapsed, 145us avg, min 23us max 4417us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 63270 events, 51632959us elapsed, 816us avg, min 283us max 6627us
io_badidle  : 0 events
io_idle     : 140682 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 140688 events, 1781435us elapsed, 12us avg, min 9us max 2945us
io_txns     : 140688 events, 47377261us elapsed, 336us avg, min 148us max 3194us
DMA allocations: 9 events
