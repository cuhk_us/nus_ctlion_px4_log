PERFORMANCE COUNTERS POST-FLIGHT

sf0x_buffer_overflows: 20688 events
sf0x_comms_errors: 6 events
sf0x_read: 20696 events, 1617772us elapsed, 78us avg, min 13us max 4108us
mavlink_txe: 88 events
mavlink_el: 269274 events, 53740666us elapsed, 199us avg, min 62us max 14248us
mc_att_control: 134222 events, 24330212us elapsed, 181us avg, min 47us max 689us
attitude_estimator_ekf: 134232 events, 109162452us elapsed, 813us avg, min 582us max 1722us
sensor task update: 134248 events, 16662319us elapsed, 124us avg, min 64us max 444us
lsm303d_accel_resched: 6602 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 67273 events, 2446861us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 537606 events, 20932761us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 511610 events, 36370878us elapsed, 71us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 5 events
adc_samples: 134560 events, 403628us elapsed, 2us avg, min 2us max 4us
ms5611_buffer_overflows: 39715 events
ms5611_comms_errors: 0 events
ms5611_measure: 52959 events, 1501033us elapsed, 28us avg, min 14us max 5275us
ms5611_read: 52958 events, 7692709us elapsed, 145us avg, min 23us max 4155us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 134218 events, 107620778us elapsed, 801us avg, min 283us max 11906us
io_badidle  : 0 events
io_idle     : 293390 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 293398 events, 3740759us elapsed, 12us avg, min 9us max 7371us
io_txns     : 293398 events, 98692529us elapsed, 336us avg, min 138us max 10662us
DMA allocations: 15 events
