PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 6229 events
sf0x_comms_errors: 6 events
sf0x_read: 6237 events, 460888us elapsed, 73us avg, min 13us max 2354us
mavlink_txe: 78 events
mavlink_el: 83799 events, 15971395us elapsed, 190us avg, min 62us max 4526us
mc_att_control: 40418 events, 7123509us elapsed, 176us avg, min 47us max 689us
attitude_estimator_ekf: 40428 events, 32340333us elapsed, 799us avg, min 582us max 1286us
sensor task update: 40444 events, 4913704us elapsed, 121us avg, min 64us max 444us
lsm303d_accel_resched: 2041 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 20269 events, 737867us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 161978 events, 6307267us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 154171 events, 10958766us elapsed, 71us avg, min 68us max 80us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 4 events
adc_samples: 40560 events, 121654us elapsed, 2us avg, min 2us max 4us
ms5611_buffer_overflows: 12135 events
ms5611_comms_errors: 0 events
ms5611_measure: 16186 events, 431933us elapsed, 26us avg, min 14us max 2032us
ms5611_read: 16185 events, 2166738us elapsed, 133us avg, min 23us max 2788us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 40414 events, 31123152us elapsed, 770us avg, min 283us max 5596us
io_badidle  : 0 events
io_idle     : 85458 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 85459 events, 1068985us elapsed, 12us avg, min 9us max 2864us
io_txns     : 85460 events, 28541605us elapsed, 333us avg, min 148us max 3219us
DMA allocations: 11 events
