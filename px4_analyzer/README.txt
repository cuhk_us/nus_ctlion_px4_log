Overview

What this software does?

Convert the *.bin file generated in pixhawk SD card to *.csv file and plot data in matlab.

Specifications

1. Replace log001.bin with your *.bin file and rename your file to log001.bin.
2. Run convert.bat to convert your log001.bin into result01.csv.
3. Run imav_load_pixhawk.m to plot the data in result01.csv.

Limitations

The conversion requires python. 