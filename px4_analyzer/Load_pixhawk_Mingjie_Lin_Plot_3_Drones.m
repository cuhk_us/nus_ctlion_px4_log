%% Load data
clear all; close all; clc;

%---Number of drones
drone_num = 3; 

%---Path name
pathname{1} = '..\log_drone1\sess071\'; % ocotrotor test, 19 Sep, fast ellipse flight test, #1, 3 drones teaming
pathname{2} = '..\log_drone2\sess171\'; % ocotrotor test, 19 Sep, fast ellipse flight test, #2, 3 drones teaming
pathname{3} = '..\log_drone3\sess113\'; % ocotrotor test, 19 Sep, fast ellipse flight test, #3, 3 drome teaming

for i = 1 : drone_num
    dataname        = 'result01'; 
    fullfilename    = [pathname{i}, dataname, '.csv']; 
    [num, txt, raw] = xlsread(fullfilename);

    data{i} = num(3:end, :);
end


%% Local position. LPOS_XYZ etc, in NED
for i = 1 : drone_num
    drone_raw{i}.pos   = data{i}(:,46:48); % NED position
    drone_raw{i}.pos_r = data{i}(:,62:64); % NED position reference
    
    %---Plot position
    figure; 
    plot(drone_raw{i}.pos); hold on; grid on; 
    plot(drone_raw{i}.pos_r); 
    legend('x', 'y', 'z', 'x_r', 'y_r', 'z_r'); title( ['Drone #' num2str(i) ', position in NED']);
end




%% Plot the trajectories with time manually alligned to take-off ("plan:0")
drone{1}.pos   = drone_raw{1}.pos(202:end,:); % NED position
drone{1}.pos_r = drone_raw{1}.pos_r(202:end,:); % NED position reference

drone{2}.pos   = drone_raw{2}.pos(253:end,:); % NED position
drone{2}.pos_r = drone_raw{2}.pos_r(253:end,:); % NED position reference

drone{3}.pos   = drone_raw{3}.pos(274:end,:); % NED position
drone{3}.pos_r = drone_raw{3}.pos_r(274:end,:); % NED position reference

hTraj =  figure; 
dist_min_ = 1000000; 

marker_list{1} = 'bo';
marker_list{2} = 'r*';
marker_list{3} = 'g^'; 

for i=1:5:min( [ size(drone{1}.pos,1), size(drone{2}.pos,1), size(drone{2}.pos,1)] )
    
    for j = 1 : drone_num
        plot3( drone{j}.pos(i,1), -drone{j}.pos(i,2), -drone{j}.pos(i,3), marker_list{j} ); hold on; grid on; 
    end
    
    dist12 = norm( drone{1}.pos(i,:) - drone{2}.pos(i,:) ); 
    dist13 = norm( drone{1}.pos(i,:) - drone{3}.pos(i,:) ); 
    dist23 = norm( drone{2}.pos(i,:) - drone{3}.pos(i,:) ); 
   
    dist_min = min([dist12, dist13, dist23]); 
    
    if dist_min < dist_min_
       dist_min_ =  dist_min;  
    end
    
    xlabel('x-North'); ylabel('y-West'); zlabel('z-Up'); legend('#1', '#2', '#3'); 
    title(['Trajectories in NWU, min dist=', num2str(dist_min)]); 
    pause(0.05); 
end
dist_min_