%% Load data
clear all; close all; clc;

%data = csvread('result01.csv', 2, 0);

%pathname = '..\log\sess055\'; % recorded UWB in vicon room, 17 Aug 2018
%pathname = '..\log\sess083\'; % ocotrotor test, 27 Aug 2018, shaking on the ground

%pathname = '..\log\sess052\'; % ocotrotor test, 3 Sep, crash in flight
%pathname = '..\log\sess055\'; % ocotrotor test, 4 Sep, ground test
%pathname = '..\log\sess058\'; % ocotrotor test, 4 Sep, ground test
%pathname = '..\log\sess068\'; % ocotrotor test, 4 Sep, ground test

%pathname = '..\log\sess003\'; % ocotrotor test, 10 Sep, ground test, #4

%% Drone #1
%pathname = '..\log_drone1\sess021\'; % ocotrotor test, 7 Sep, #1, crashed in teaming
%pathname = '..\log_drone1\sess036\'; % ocotrotor test, 11 Sep, ground test, #1,  laser no reading
%pathname = '..\log_drone1\sess037\'; % ocotrotor test, 11 Sep, #1, manual flight test, laser not reliable
%pathname = '..\log_drone1\sess038\'; % ocotrotor test, 11 Sep, manual flight test, #1, after change the laser range finder
%pathname = '..\log_drone1\sess044\'; % ocotrotor test, 11 Sep, flight test, #1, teaming
%pathname = '..\log_drone1\sess059\'; % ocotrotor test, 18 Sep, flight test, #1, ellipse
%pathname = '..\log_drone1\sess062\'; % ocotrotor test, 18 Sep, flight test, #1, ellipse
%pathname = '..\log_drone1\sess065\'; % ocotrotor test, 19 Sep, extreme fast
%pathname = '..\log_drone1\sess071\'; % ocotrotor test, 19 Sep, fast ellipse flight test, #1, 3 drones teaming
%pathname = '..\log_drone1\sess078\'; % ocotrotor test, 21 Sep, fast ellipse flight test, #1, 3 drones teaming, trial 1
%pathname = '..\log_drone1\sess079\'; % ocotrotor test, 21 Sep, fast ellipse flight test, #1, 3 drones teaming, trial 2
%pathname = '..\log_drone1\sess096\'; % ocotrotor test, 25 Sep, fast ellipse flight test, #1, 3 drones teaming, trial 1
%pathname = '..\log_drone1\sess097\'; % ocotrotor test, 25 Sep, fast ellipse flight test, #1, 3 drones teaming, trial 2


%% Drone #2
%pathname = '..\log_drone2\sess147\'; % ocotrotor test, 11 Sep, flight test, #2, teaming
%pathname = '..\log_drone2\sess171\'; % ocotrotor test, 19 Sep, fast ellipse flight test, #2, 3 drones teaming
%pathname = '..\log_drone2\sess179\'; % ocotrotor test, 21 Sep, fast ellipse flight test, #2, 3 drones teaming, trial 1
%pathname = '..\log_drone2\sess180\'; % ocotrotor test, 21 Sep, fast ellipse flight test, #2, 3 drones teaming, trial 2
%pathname = '..\log_drone2\sess192\'; % ocotrotor test, 25 Sep, fast ellipse flight test, #2, 3 drones teaming, trial 1
%pathname = '..\log_drone2\sess193\'; % ocotrotor test, 25 Sep, fast ellipse flight test, #2, 3 drones teaming, trial 2


%% Drone #3
%pathname = '..\log_drone3\sess077\'; % ocotrotor test, 7 Sep, fast fligh,#3
%pathname = '..\log_drone3\sess078\'; % ocotrotor test, 7 Sep, fligh test, #3, way point flight, triangle
%pathname = '..\log_drone3\sess079\'; % ocotrotor test, 7 Sep, fligh test, #3
%pathname = '..\log_drone3\sess084\'; % ocotrotor test, 11 Sep, flight test, #3, teaming
%pathname = '..\log_drone3\sess098\'; % ocotrotor test, 13 Sep, flight test, #3, circling
%pathname = '..\log_drone3\sess113\'; % ocotrotor test, 19 Sep, fast ellipse flight test, #3, 3 drome teaming
%pathname = '..\log_drone3\sess126\'; % ocotrotor test, 21 Sep, fast ellipse flight test, #3, 3 drome teaming, trail 1
%pathname = '..\log_drone3\sess127\'; % ocotrotor test, 21 Sep, fast ellipse flight test, #3, 3 drome teaming, trail 2
%pathname = '..\log_drone3\sess143\'; % ocotrotor test, 25 Sep, fast ellipse flight test, #3, 3 drome teaming, trail 1
%pathname = '..\log_drone3\sess144\'; % ocotrotor test, 25 Sep, fast ellipse flight test, #3, 3 drome teaming, trail 2

%% Drone #4
%pathname = '..\log_drone4\sess004\'; % ocotrotor test, 11 Sep, ground test, #4, ground test, laser not reliable
%pathname = '..\log_drone4\sess005\'; % ocotrotor test, 12 Sep, ground test, #4, ground test, laser not reliable
%pathname = '..\log_drone4\sess007\'; % ocotrotor test, 12 Sep, flight test, #4, ground test, laser not reliable

%pathname = '..\log_drone4\sess025\'; % 21 Sep, ground, #4, laser range finder test
%pathname = '..\log_drone4\sess026\'; % 21 Sep, ground, #4, laser range finder test
%pathname = '..\log_drone4\sess027\'; % 21 Sep, ground, #4, without laser correction
%pathname = '..\log_drone4\sess028\'; % 21 Sep, ground, #4, laser range finder test

%pathname = '..\log_drone4\sess014\'; % ocotrotor test, 21 Sep, flight test, #4, laser range finder test
%pathname = '..\log_drone4\sess015\'; % ocotrotor test, 21 Sep, flight test, #4, laser range finder test


%% Drone #5
%pathname = '..\log_drone5\sess016\'; % ocotrotor test, 2 Oct, flight test, #5, using traj2
%pathname = '..\log_drone5\sess017\'; % ocotrotor test, 2 Oct, flight test, #5, using traj1
pathname = '..\log_drone5\sess018\'; % ocotrotor test, 3 Oct, flight test, #5, in vicon room, hover->left 90 deg->hover->right 90 deg


dataname        = 'result01'; 
fullfilename    = [pathname, dataname, '.csv']; 
[num, txt, raw] = xlsread(fullfilename);

data = num(3:end, :);

%% run time
t         = data(:,340)*0.000001; % time
freq      = 50;
startTimeIndex = 1; 
t_min     = t(startTimeIndex);
t_max     = max(t);
ind_plot  = floor(t_min*freq):size(t,1);

%% attitude measurement
phi = data(:,1); % roll angle
tht = data(:,2); % pitch angle
psi = data(:,3); % yaw angle

p   = data(:,4); % roll anglular rate
q   = data(:,5); % pitch anglular rate
r   = data(:,6); % yaw anglular rate

phi_r = data(:,10); % roll reference
tht_r = data(:,11); % pitch reference
psi_r = data(:,12); % yaw reference
thr_r = data(:,13); % thrust reference

%% imu raw data
acx = data(:,14); % imu x acceleration
acy = data(:,15); % imu y acceleration
acz = data(:,16); % imu z acceleartion

p_raw = data(:,17); % roll rate raw
q_raw = data(:,18); % pitch rate raw
r_raw = data(:,19); % yaw rate raw

mag_x = data(:,20); % magnetometer x
mag_y = data(:,21); % magnetometer y
mag_z = data(:,22); % magnetometer z

%% baro data
p_baro = data(:,41); % baro pressure
h_baro = data(:,42); % baro height
h_baro = - ( h_baro - h_baro(1) ); 

temp_baro =  data(:,43); % baro temperature

%% Local position. LPOS_XYZ etc.
x = data(:,46); % N position
y = data(:,47); % E position
z = data(:,48); % D position

z_range_finder = - data(:,49);
diff_range_kf = data(:,49) + data(:,48);

u = data(:,51); % N velocity 
v = data(:,52); % E velocity 
w = data(:,53); % D velocity 

% local position set point
x_r = data(:,62); % N position reference
y_r = data(:,63); % E position reference
z_r = data(:,64); % D position reference

% imav nuc reference
ref_x=data(:,62);
ref_y=data(:,63);
ref_z=data(:,64);
ref_yaw=data(:,336);

%% GPS coonfiguration data
fix = data(:,67); % GPS fix
eph = data(:,68); % GPS EPH
epv = data(:,69); % GPS EPV

lat = data(:,70); % GPS latitude
lon = data(:,71); % GPS longitude
alt = data(:,72); % GPS altitude

v_N = data(:,73); % GPS latitude
v_E = data(:,74); % GPS longitude
v_D = data(:,75); % GPS altitude

%% attitude control
ail = data(:,81); % aileron
ele = data(:,82); % elevator
rud = data(:,83); % rudder
thr = data(:,84); % throttle

%% flight state
stat_main = data(:,85); % main state
stat_arm  = data(:,86); % arming state
stat_fail = data(:,87); % failsafe state
stat_batr = data(:,88); % battery rem state
stat_batw = data(:,89); % battery warn state
stat_land = data(:,90); % landed state
stat_nav  = data(:,91); % navigation state

%% control signal
RC1 = data(:,92); % Aileron
RC2 = data(:,93); % Elevator
RC3 = data(:,94); % Throttle
RC4 = data(:,95); % Rudder
RC5 = data(:,96); % Main mode switch
RC6 = data(:,97); % Loiter switch
RC7 = data(:,98); % Return home switch

%% global velosity setpoint
u_r = data(:,153); % N velocity reference
v_r = data(:,154); % E velocity reference
w_r = data(:,155); % D velocity reference

%%
out1 = data(:,102);
out2 = data(:,103);
out3 = data(:,104);
out4 = data(:,105);
out5 = data(:,106);
out6 = data(:,107);
out7 = data(:,108);
out8 = data(:,109);

thr_sp = (out1+out2+out3+out4)/4;

rate_sp_roll = data(:,113);
rate_sp_pitch = data(:,114);
rate_sp_yaw = data(:,115);

triggerFlags = data(:,329);

%% GPS lat measurement
% figure
% subplot(211); plot(t, lat,'r'); xlim([t_min t_max]); legend('lat'); title('Latitude')
% subplot(212); 
% plot(t,x,'.-b', t, u, 'g', t, v_N, 'r','LineWidth',1.5);  xlim([t_min t_max]); legend('x','u','v-N')
% title('x & u & v-N')


% figure
% plot(lat(startTimeIndex:end),lon(startTimeIndex:end)); 

%% 2D trajectory
% figure
% plot(x(startTimeIndex:end),y(startTimeIndex:end)); title('2D trajectory'); 
% xlabel('x-north (m)'); ylabel('y-east (m)'); grid on; axis equal; 


%% 3D trajectory
figure
plot3(x(startTimeIndex:end),y(startTimeIndex:end), -z(startTimeIndex:end), 'r', 'linewidth', 2); title('3D trajectory'); 
xlabel('x-north (m)'); ylabel('y-east (m)'); zlabel('z-up (m)'); 
grid on; axis equal; 

%% GPS lon measurement
% figure
% subplot(211); plot(t, lon,'r'); xlim([t_min t_max]); legend('lon');title('Longitude');xlabel('time (s)');
% subplot(212); 
% plot(t,y,'.-b', t, v, 'g', t, v_E, 'r','LineWidth',1.5);  xlim([t_min t_max]); legend('y','v','v-E');xlabel('time (s)'); 
% title('y & v & v-E')

%% GPS height measurement
 figure
 plot(t, alt, 'r',t,z, '.-b', t, h_baro,'g', t, z_range_finder, 'k', t, diff_range_kf, 'c'); xlim([t_min t_max]); title('Height Estimationc');xlabel('time (s)'); 
 legend('alt','z', 'h-baro', 'Range Finder', 'Estimation Error')

% figure
% %  plot(t, z, 'LineWidth',1.5);  xlim([t_min t_max]);
% % legend('z');xlabel('time (s)');  title('Z-axis pos estimate');
% subplot(212); plot(t, w, 'g', t, v_D, 'r','LineWidth',1.5);  xlim([t_min t_max]); legend('w','v-D');title('Z-axis vel estimate');xlabel('time (s)');

%% Acceleration Plot
% figure; 
% subplot(3,1,1); plot(t,acx); grid on; title('x-axis'); xlim([t_min t_max]);legend('acx'); title('X Acceleration')
% subplot(3,1,2); plot(t,acy); grid on; title('y-axis'); xlim([t_min t_max]);legend('acy'); title('Y Acceleration')
% subplot(3,1,3); plot(t,acz); grid on; title('z-axis'); xlim([t_min t_max]);legend('acz'); title('Z Acceleration')

%% X Y Z H : Reference tracking for control
figure; 
subplot(2,2,1); plot(t,x,'.b',t,ref_x,'r.', t,u,'.g',t,u_r,'k'); xlim([t_min t_max]); set(gca,'FontSize',14); grid on;
title('x-axis','FontSize',14); legend('x','x-ref','ug','ug-ref'); xlabel('time (s)'); ylabel('x-axis p & v (m & m/s)');

subplot(2,2,2); plot(t,y,'.b',t,ref_y,'r.', t,v,'.g',t,v_r,'k'); xlim([t_min t_max]); set(gca,'FontSize',14); grid on;
title('y-axis','FontSize',14); legend('y','y-ref','vg','vg-ref'); xlabel('time (s)'); ylabel('y-axis p & v (m & m/s)');

subplot(2,2,3); plot(t,z,'.b',t, ref_z,'r.');  xlim([t_min t_max]); set(gca,'FontSize',14); grid on;
title('z-axis','FontSize',14); legend('z','z-ref','wg','wg-ref'); xlabel('time (s)'); ylabel('z-axis p & v (m & m/s)');
%, t, w,'.g', t, w_r,'k'
subplot(2,2,4); plot(t,psi*180/pi,'.b',t,psi_r*180/pi, 'r.'); xlim([t_min t_max]); set(gca,'FontSize',14); grid on;
title('Heading','FontSize',14); legend('\psi','\psi-ref'); xlabel('time (s)'); ylabel('Yaw angle (deg)'); 

%% %% Attitude control performance
figure;
subplot(3,2,1); plot(t, phi_r, 'r', t, phi, 'b');   xlim([t_min t_max]); ylabel('\phi (rad)'); grid on; legend('ref', 'mea')
subplot(3,2,3); plot(t, p, 'b',t,rate_sp_roll,'r'); xlim([t_min t_max]); ylabel('p (rad/s)'); grid on;
subplot(3,2,5); plot(t, ail, 'b'); xlim([t_min t_max]); ylabel('\delta_{ail} [-1 1]'); grid on;

subplot(3,2,2); plot(t, tht_r, 'r', t, tht, 'b'); xlim([t_min t_max]); ylabel('\theta (rad)'); grid on;
subplot(3,2,4); plot(t, q, 'b',t,rate_sp_pitch,'r'); xlim([t_min t_max]); ylabel('q (rad/s)'); grid on;
subplot(3,2,6); plot(t, ele, 'b'); xlim([t_min t_max]); ylabel('\delta_{ele} [-1 1]'); grid on;

vel = [u, v, w]; 
vel_max = max( sqrt( sum( vel.^2, 2) ) )

%% reference
figure;
plot(t, tht_r); 

%% heading
figure;
subplot(2,1,1), plot(t, r_raw, 'r', t, psi, 'b'); xlim([t_min t_max]); ylabel('yaw (rad) and yaw rate (rad/s)'); grid on;
subplot(2,1,2), plot(t, rud); xlim([t_min t_max]); ylabel('rudder'); grid on;


%% Mode switches
figure;
plot(t, RC5, 'b', t, RC6, 'r', t, RC7, 'k'); ylim([-1.5 1.5]); legend('Mode switch', 'Loiter switch', 'Return home'); xlabel('time (s)'); grid on;

%% RC plot
figure; 
subplot(4,2,1); plot(t, RC1); ylabel('RC1'); grid on;
subplot(4,2,3); plot(t, RC2); ylabel('RC2'); grid on;
subplot(4,2,5); plot(t, RC3); ylabel('RC3'); grid on;
subplot(4,2,7); plot(t, RC4); ylabel('RC4'); grid on;

subplot(4,2,2); plot(t, RC5); ylabel('RC5'); grid on;
subplot(4,2,4); plot(t, RC6); ylabel('RC6'); grid on;
subplot(4,2,6); plot(t, RC7); ylabel('RC7'); grid on;

%%
figure; 
subplot(5,1,1); plot(t,stat_main); ylabel('stat\_main');
subplot(5,1,2); plot(t,stat_nav); ylabel('stat\_nav');
subplot(5,1,3); plot(t,stat_land); ylabel('stat\_land');
subplot(5,1,4); plot(t,stat_arm); ylabel('stat\_arm');
subplot(5,1,5); plot(t,triggerFlags); ylabel('triggerFlags');

%% output plot
figure; 
hold on
plot(t,out1,'r');ylabel('Out1'); grid on;
plot(t,out2,'g');ylabel('Out2'); grid on;
plot(t,out3,'b');ylabel('Out3'); grid on;
plot(t,out4,'k');ylabel('Out4'); grid on;
title('RC PWM output')
legend('Out1','Out2','Out3','Out4');

plot(t,out5,'g.');ylabel('Out5');
plot(t,out6,'b.');ylabel('Out6');
plot(t,out7,'y.');ylabel('Out7');
plot(t,out8,'y');ylabel('Out8');
legend('Out1','Out2','Out3','Out4','Out5','Out6','out7','out8');

% 
% p_dot = zeros(size(t));
% for i = 1:size(t,1)-1
%    p_dot(i) =  (p(i+1)-p(i))/(t(i+1)-t(i));  
% end



% % Comparison of positions from VO and GPS
% % spheroid = referenceEllipsoid('GRS 80'); 
% spheroid = wgs84Ellipsoid;
% [x_gps,y_gps,z_gps] = geodetic2ned(lat,lon,alt,lat(1),lon(1),alt(1),spheroid);
% 
% % rotate gps position a certain angle to compensate the error caused by
% % magnetometer
% Rz          = rotz(0); % in degree
% xyz_gps_Rz  = Rz * [x_gps, y_gps, z_gps]'; 
% 
% x_gps_Rz = xyz_gps_Rz(1,:)';
% y_gps_Rz = xyz_gps_Rz(2,:)';
% z_gps_Rz = xyz_gps_Rz(3,:)';
% 
% %% position comparison
% figure;
% subplot(3,1,1);
% plot(t, x_gps_Rz, 'b', t, x, 'r', 'linewidth', 2); 
% grid on; axis tight; set(gca, 'fontsize', 10); 
% ylabel('North (m)'); legend('GPS', 'VO'); title('Position in NED'); 
% 
% subplot(3,1,2);
% plot(t, y_gps_Rz, 'b', t, y, 'r', 'linewidth', 2); 
% grid on; axis tight; set(gca, 'fontsize', 10); 
% ylabel('East (m)');
% 
% subplot(3,1,3);
% plot(t, z_gps_Rz, 'b', t, z-z(1), 'r', 'linewidth', 2); 
% grid on; axis tight; set(gca, 'fontsize', 10); 
% ylabel('Down (m)'); xlabel('time (s)'); 


% %% velocity comparison
% figure;
% subplot(3,1,1);
% plot(t, v_N, 'b', t, u, 'r', 'linewidth', 2); 
% grid on; axis tight; set(gca, 'fontsize', 10); 
% ylabel('North (m/s)'); legend('GPS', 'VO'); title('Velocity in NED'); 
% 
% subplot(3,1,2);
% plot(t, v_E, 'b', t, v, 'r', 'linewidth', 2); 
% grid on; axis tight; set(gca, 'fontsize', 10); 
% ylabel('East (m/s)');
% 
% subplot(3,1,3);
% plot(t, v_D, 'b', t, w, 'r', 'linewidth', 2); 
% grid on; axis tight; set(gca, 'fontsize', 10); 
% ylabel('Down (m/s)'); xlabel('time (s)'); 


% figure;
% %t1 = 500;
% %t2 = 1372;
% t1 = 1;
% t2 = length(x);
% plot(y_gps_Rz(t1:t2), x_gps_Rz(t1:t2), 'b', y(t1:t2), x(t1:t2), 'r'); axis equal;
% 
% figure;
% plot(y, x, 'r', 'linewidth', 2); axis equal; 
% set(gca, 'fontsize', 10); 
% axis tight; grid on; 
% xlabel('East (m)'); ylabel('North'); 