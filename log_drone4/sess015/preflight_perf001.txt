PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 0 events
sf0x_comms_errors: 12 events
sf0x_read: 12 events, 300us elapsed, 25us avg, min 12us max 158us
mavlink_txe: 17 events
mavlink_el: 61 events, 7222us elapsed, 118us avg, min 65us max 1102us
mc_att_control: 103 events, 13795us elapsed, 133us avg, min 48us max 710us
attitude_estimator_ekf: 111 events, 84399us elapsed, 760us avg, min 587us max 1148us
sensor task update: 127 events, 14177us elapsed, 111us avg, min 70us max 261us
lsm303d_accel_resched: 87 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 69 events, 2503us elapsed, 36us avg, min 35us max 37us
lsm303d_accel_read: 546 events, 21081us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 556 events, 39385us elapsed, 70us avg, min 68us max 75us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 1 events
adc_samples: 150 events, 449us elapsed, 2us avg, min 2us max 4us
ms5611_buffer_overflows: 37 events
ms5611_comms_errors: 0 events
ms5611_measure: 57 events, 939us elapsed, 16us avg, min 14us max 91us
ms5611_read: 56 events, 4785us elapsed, 85us avg, min 23us max 171us
io rc #: 0 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 96 events, 80463us elapsed, 838us avg, min 284us max 5378us
io_badidle  : 0 events
io_idle     : 258 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 260 events, 2982us elapsed, 11us avg, min 8us max 148us
io_txns     : 260 events, 99841us elapsed, 384us avg, min 148us max 1948us
DMA allocations: 8 events
