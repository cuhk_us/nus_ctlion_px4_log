PERFORMANCE COUNTERS PRE-FLIGHT

sf0x_buffer_overflows: 11 events
sf0x_comms_errors: 6 events
sf0x_read: 19 events, 737us elapsed, 38us avg, min 12us max 139us
mavlink_txe: 52 events
mavlink_el: 167 events, 24978us elapsed, 149us avg, min 62us max 1552us
mc_att_control: 154 events, 23223us elapsed, 150us avg, min 46us max 654us
attitude_estimator_ekf: 163 events, 122600us elapsed, 752us avg, min 589us max 1073us
sensor task update: 179 events, 24195us elapsed, 135us avg, min 64us max 736us
lsm303d_accel_resched: 141 events
lsm303d_extremes: 0 events
lsm303d_reg7_resets: 0 events
lsm303d_reg1_resets: 0 events
lsm303d_mag_read: 96 events, 3448us elapsed, 35us avg, min 35us max 37us
lsm303d_accel_read: 753 events, 28810us elapsed, 38us avg, min 38us max 39us
mpu6000_bad_transfers: 0 events
mpu6000_read: 757 events, 53568us elapsed, 70us avg, min 68us max 75us
mpu6000_gyro_read: 0 events
mpu6000_accel_read: 1 events
adc_samples: 210 events, 624us elapsed, 2us avg, min 2us max 4us
ms5611_buffer_overflows: 54 events
ms5611_comms_errors: 0 events
ms5611_measure: 78 events, 1248us elapsed, 16us avg, min 14us max 91us
ms5611_read: 77 events, 10412us elapsed, 135us avg, min 23us max 1433us
io rc #: 1 events
io write: 0 events, 0us elapsed, 0us avg, min 0us max 0us
io update: 149 events, 161880us elapsed, 1086us avg, min 283us max 12764us
io_badidle  : 0 events
io_idle     : 427 events
io_uarterrs : 0 events
io_protoerrs: 0 events
io_dmaerrs  : 0 events
io_crcerrs  : 0 events
io_timeouts : 0 events
io_retries  : 0 events
io_dmasetup : 428 events, 4965us elapsed, 11us avg, min 9us max 102us
io_txns     : 428 events, 177177us elapsed, 413us avg, min 148us max 11194us
DMA allocations: 8 events
